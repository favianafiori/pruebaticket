<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Ticket extends Model
{
    protected $fillable = ['user_id', 'nombre', 'descripcion'];

    public function saveTicket($data)
    {
        $this->user_id = auth()->user()->id;
        $this->nombre = $data['nombre'];
        $this->descripcion = $data['descripcion'];
        $this->nivel_importancia = $data['nivel_importancia'];
        $this->save();
        return 1;
    }

    public function updateTicket($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->user_id = auth()->user()->id;
        $ticket->nombre = $data['nombre'];
        $ticket->descripcion = $data['descripcion'];
        $this->nivel_importancia = $data['nivel_importancia'];
        $ticket->save();
        return 1;
    }

    //nuevo para filtros
    public function scopeNombre($query, $nombre)
    {
        if($nombre)
            return $query->where('nombre','LIKE', "%$nombre%");
    }

    public function scopeDescripcion($query, $descripcion)
    {
        if($descripcion)
            return $query->where('descripcion','LIKE', "%$descripcion%");
    }

    public function scopeNivel_Importancia($query, $nivel_importancia)
    {
        if($nivel_importancia)
            return $query->where('nivel_importancia','LIKE', "%$nivel_importancia%");
    }
}
