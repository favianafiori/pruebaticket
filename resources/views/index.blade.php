@extends('layouts.app')

@section('content')
<div class="container">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- nuevo -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>
                        Gestión de Tickets
                        {{Form::open (['route'=> 'tickets', 'method' => 'GET', 'class'=> 'form-inline pull-right'])}}
                            <div class="form-group">
                                {{Form::text('nombre', null, ['class'=> 'form-control', 'placeholder' => 'Nombre'])}}
                            </div>
                            <div class="form-group">
                                {{Form::text('descripcion', null, ['class'=> 'form-control', 'placeholder' => 'Descripción'])}}
                                </div>
                            <div class="form-group">
                                {{Form::text('nivel_importancia', null, ['class'=> 'form-control', 'placeholder' => 'Nivel Importancia'])}}
                            </div>
                            <div class="form-group">
                                    <button type="submit" class = "btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                            </div>

                        {{Form::close()}}
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
              <td>ID</td>
              <td>Nombre</td>
              <td>Descripcion</td>
              <td>Importancia</td>
              <td colspan="2">Action</td>
            </tr>
        </thead>
        <tbody>
            @foreach($tickets as $ticket)
            <tr>
                <td>{{$ticket->id}}</td>
                <td>{{$ticket->nombre}}</td>
                <td>{{$ticket->descripcion}}</td>
                <td>{{$ticket->nivel_importancia}}</td>
                <td><a href="{{action('TicketController@edit',$ticket->id)}}" class="btn btn-primary">Editar</a></td>
                <td>
                    <form action="{{action('TicketController@destroy', $ticket->id)}}" method="post">
                    <input type="hidden" value="{{csrf_token()}}" name="_token" />
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
<div>
@endsection