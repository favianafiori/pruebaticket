@extends('layouts.app')

@section('content')
<div class="container">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
@endif
    <h2>Registrar Ticket</h2>
    <div class="row">
    <form method="post" action="{{url('/create/ticket')}}">
        <div class="form-group">
            <input type="hidden" value="{{csrf_token()}}" name="_token" />
            <label for="nombre">Nombre:</label>
            <input type="text" class="form-control" name="nombre"/>
        </div>
        <div class="form-group">
            <label for="descripcion">Descripcion:</label>
            <textarea cols="5" rows="5" class="form-control" name="descripcion"></textarea>
        </div>
        <div class="form-group">
                <label for="nivel_importancia">Nivel Importancia</label>
                <select class="form-control" name="nivel_importancia">
                  <option>Seleccione una opción</option>
                  <option value="Urgente">Urgente</option>
                  <option value="Medio">Medio</option>
                  <option value="Bajo">Bajo</option>
                </select>
              </div>
        <button type="submit" class="btn btn-primary">Registrar</button>
        </form>
    </div>
</div>
@endsection